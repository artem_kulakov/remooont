$(document).ready(function() {
    //initialize swiper when document ready
	var swiper = new Swiper('.swiper-container', {
		navigation: {
		  nextEl: '.swiper-button-next',
		  prevEl: '.swiper-button-prev',
		},
		loop: true,
		pagination: {
	        el: '.swiper-pagination',
		},
		scrollbar: {
			el: '.swiper-scrollbar',
			hide: false,
		},
	});
});