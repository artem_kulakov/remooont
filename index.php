<?php
require __DIR__ . "/vendor/autoload.php";

$haml = new MtHaml\Environment('php');
$executor = new MtHaml\Support\Php\Executor($haml, array(
    'cache' => sys_get_temp_dir().'/haml',
));

// Compiles and executes the HAML template, with variables given as second
// argument
$file = basename(__FILE__, '.php') . '.haml';

$executor->display($file, array(
    'foo' => 'bar',
));
?>